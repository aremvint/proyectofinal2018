

/****Ojo*////////
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <signal.h>
#include <dirent.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "logdb.h"

#include "hashtable.h"

#define MAXIMO 1000
#define TAMANO_HT 347



//#include "hashtable.h"
#include "csapp.h"


typedef struct{
    char *nombre_base;
    hashtable *ht;
    char *indice_base;
    FILE *f;
    hashtable *ht_acumuladora;
    hashtable *ht_temporal;
    sem_t semaforo;
    FILE *nuevo_archivo;
    volatile int compactado;
} base_servidor;


int crear_base_servidor(char *archivo);

base_servidor *abrir_base_servidor(char *archivo_base, char *indice_base);

int put_servidor(base_servidor *db, char *clave, char *valor);

char *get_servidor(base_servidor *db, char *clave);

int cerrar_conexion_servidor(base_servidor *db);

void compactar_servidor(base_servidor *db);

void fin_compactar(base_servidor *db);
