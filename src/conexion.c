#include "logdb.h"//libreria de funciones del cliente

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>


conexionlogdb *conectar_db(char *ip, int puerto)
{
	conexionlogdb* conectar = (conexionlogdb*) malloc(sizeof(conexionlogdb));
	int  conexion;
	//srand(time(NULL));
	
	struct sockaddr_in cliente;
	//struct sockaddr_in sockServer; //Declaración de la estructura con información para la conexión
	  struct hostent *servidor; //Declaración de la estructura con información del host
	  servidor = gethostbyname(ip); //Asignacion
	  if(servidor == NULL)
	  { //Comprobación
	    printf("Host erróneo\n");
	    return NULL;
	  }

	conexion = socket(AF_INET, SOCK_STREAM, 0);
	bzero((char *)&cliente, sizeof((char *)&cliente)); //Rellena toda la estructura de 0's
        //La función bzero() es como memset() pero inicializando a 0 todas la variables
  cliente.sin_family = AF_INET; //asignacion del protocolo
  cliente.sin_port = htons(puerto); //asignacion del puerto
  bcopy((char *)servidor->h_addr, (char *)&cliente.sin_addr.s_addr, sizeof(servidor->h_length));
  //bcopy(); copia los datos del primer elemendo en el segundo con el tamaño máximo 
  //del tercer argumento.

	
	 if(connect(conexion,(struct sockaddr *)&cliente, sizeof(cliente)) < 0)
  { //conectando con el host
    printf("Error conectando con el host\n");
    close(conexion);
    return NULL;
  }
	 printf("Conectado con %s:%d\n",inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));
	
	conectar->ip = ip;
	conectar->puerto = puerto;
	
	conectar->sockdf = conexion;
	conectar->id_sesion =1;//se crea un identificador al alzar
	
	return conectar;
}

