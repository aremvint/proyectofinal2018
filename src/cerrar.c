#include "logdb.h"//libreria de funciones del cliente

void cerrar_db(conexionlogdb *conexion)
{
	close(conexion->sockdf);
	free(conexion->sockdf);
	free(conexion->ip);
	free(conexion->puerto);
	free(conexion->id_sesion);
	free(conexion->nombredb);

	free(conexion);
	
}

