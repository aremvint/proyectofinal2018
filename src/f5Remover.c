#include "hashtable.h"
// //Remueve la clave/valor del la hashtable, y devuelve el valor asociado a la clave

void *remover(hashtable *tabla, char *clave){
	if(!tabla || !clave)
		return NULL;

	unsigned long i = hash((unsigned char *)clave) % tabla->numeroBuckets;
	objeto * elemento = tabla->buckets[(int)i];
	objeto * elementoPrevio = NULL;
	
	
	//if (clave==NULL) return NULL;
	while(elemento != NULL){
		if(!strcmp(elemento->clave, clave))
		{
			void *dato = elemento->valor;//ojo
			if(elementoPrevio!=NULL)
				elementoPrevio->siguiente = elemento->siguiente;
			else
				tabla->buckets[(int)i] = elemento->siguiente;
			free(elemento->clave);
			elemento->clave=NULL;

			//free(elemento);
		
			//elemento = NULL;
			tabla->elementos--;
			return dato;//ojo
			
		}

		elementoPrevio = elemento;
		elemento = elemento->siguiente;

	}
	
	return NULL;
}
