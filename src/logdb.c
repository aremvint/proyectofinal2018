#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<netdb.h>
#include "hashtable.h"
 


#include <time.h>




void MinToMay(char string[]);

int main(int argc, char **argv){
  int pid;
  char valor[1];//para enviar valores puntuales por sockets
  if(argc<2)
  { //Especifica los argumentos
    printf("%s <host> <puerto>\n",argv[0]);
    return 1;
  }
  int conexion_servidor, conexion_cliente, puerto; //declaramos las variables
  socklen_t longc; //Debemos declarar una variable que contendrá la longitud de la estructura
  struct sockaddr_in servidor, cliente;
  char buffer[100]; //Declaramos una variable que contendrá los mensajes que recibamos
  puerto = atoi(argv[2]);
  conexion_servidor = socket(AF_INET, SOCK_STREAM, 0); //creamos el socket
  bzero((char *)&servidor, sizeof(servidor)); //llenamos la estructura de 0's
  servidor.sin_family = AF_INET; //asignamos a la estructura
  servidor.sin_port = htons(puerto);
  servidor.sin_addr.s_addr = INADDR_ANY; //esta macro especifica nuestra dirección
  if(bind(conexion_servidor, (struct sockaddr *)&servidor, sizeof(servidor)) < 0)
  { //asignamos un puerto al socket
    printf("Error al asociar el puerto a la conexion\n");
    close(conexion_servidor);
    return 1;
  }
  listen(conexion_servidor, 3); //Estamos a la escucha
  printf("A la escucha en el puerto %d\n", ntohs(servidor.sin_port));
  longc = sizeof(cliente); //Asignamos el tamaño de la estructura a esta variable
  while(1){
  conexion_cliente = accept(conexion_servidor, (struct sockaddr *)&cliente, &longc); //Esperamos una conexion

  if(conexion_cliente<0)
  {
    printf("Error al aceptar trafico\n");
    close(conexion_servidor);
    return 1;
  }
  printf("Conectando con %s:%d\n", inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));
  if(recv(conexion_cliente, buffer, 100, 0) < 0)
  { //Comenzamos a recibir datos del cliente
    //Si recv() recibe 0 el cliente ha cerrado la conexion. Si es menor que 0 ha habido algún error.
    printf("Error al recibir los datos\n");
    close(conexion_servidor);
    return 1;
  }
  else
  {
    printf("%s\n", buffer);
	MinToMay(buffer);
	printf("La palabra en mayuscula es: %s \n", buffer);
   // bzero((char *)&buffer, sizeof(buffer));
    send(conexion_cliente,buffer , 100, 0);//envio de vuelta el buffer convertido a mayuscula
	//**************HARE RECIBOS Y ENVIOS************************
	bzero(buffer, 100);


	/*****PARA CREAR BASE*///////
	int archivo;
	recv(conexion_cliente, buffer, 100, 0);
		
		valor[0]=1;
	send(conexion_cliente, valor[0], 100, 0);

	bzero(buffer, 100);
	/******PARA ABRIR BASE DE DATOS*/
	recv(conexion_cliente, buffer, 100,0);
	
		valor[0]=1;
	send(conexion_cliente, valor[0], 100, 0);
	bzero(buffer, 100);


	/**********INGRESAR ELEMENTOS EN BASE DE DATOS*/
	
	/**********OBTENER ELEMENTOS DE LA BASE DE DATOS*/
	/**********ELIMINAR ELEMENTOS DE LA BASE DE DATOS*/
	/**********COMPACTAR******/
	recv(conexion_cliente, buffer, 100, 0);
	

	//**************FIN DE RECIBOS Y ENVIOS**********************

  }
  pid = fork();
  if(pid<0)
	return 1;
  if(pid==0){
	close(conexion_cliente);
	//dostuff(conexion_cliente);
	exit(0);
	}
	else{
		close(conexion_cliente);
	}
  close(conexion_cliente);
  }


  close(conexion_servidor);
  return 0;
}




void MinToMay(char string[])
{
	int i=0;
	int desp='a'-'A';
	for (i=0;string[i]!='\0';++i)
	{
		if(string[i]>='a'&&string[i]<='z')
		{
			string[i]=string[i]-desp;
		}
	}
}
