#include "hashtable.h"
int contieneClave(hashtable *tabla, char *clave){
	/*objeto *elemento;
	for(int i =0;i<numeroElementos(tabla);i++){
		elemento = tabla->buckets[i];
		if(elemento!=NULL){
			if(!strcmp(clave, elemento->clave)==0)
				return 1;
			
		}
	}
	
	return 0;*/

	unsigned long i = hash((unsigned char *)clave) % tabla->numeroBuckets;
	objeto * elemento = tabla->buckets[(int)i];
	while(elemento != NULL){
		if(!strcmp(elemento->clave, clave))
		{
			return 1;
			
		}

		elemento = elemento->siguiente;

	}
	
	return 0;
}
