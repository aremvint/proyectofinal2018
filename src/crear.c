#include "logdb.h"//libreria de funciones del cliente
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int crear_db(conexionlogdb *conexion, char *nombre_db)
{
	char respuesta[1];//recebira la respuesta del servidor
	int valor_respuesta=0;
	int archivo;
	/*char accion[MAXLINE] = "c ";
	
	strcat(accion,conexion->nombredb);
	strcat(accion,"\n");*/

	if(nombre_db==NULL){
		return 0;
	}
	else{
		umask(0);
		//create con permiso 666
	 	
		
		 send(conexion->sockdf, nombre_db, 100, 0); //envioel nombre del archivo al servidor para verificar que este no este creado antes
  		//bzero(buffer, 100);
		recv(conexion->sockdf, respuesta , 100, 0);
		 valor_respuesta=atoi(respuesta[0]);
		if(valor_respuesta==	0)
			return 0;
		archivo = creat(nombre_db, O_RDWR|O_CREAT|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
		close(archivo);
		//poner condicional para ver si existe el archivo antes de crearlo
		strcpy(conexion->nombredb, nombre_db);
	}	

	return 1;	
	
}
