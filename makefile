IDIR =./include
CC=gcc
CFLAGS=-I$(IDIR) -Wall

ODIR=obj

LIBS=-lm -lpthread

_OBJ = abrir.o cerrar.o compactar.o conexion.o crear_db.o eliminar.o get.o put.o funciones_servidor.o csapp.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


all: bin/prueba lib/liblogdb.so bin/logdb

$(ODIR)/%.o: src/%.c $(DEPS)
	$(CC) -c -fPIC -o $@ $< $(CFLAGS)



bin/logdb:	obj/logdb.o lib/csapp.so lib/libhashtab.so
		gcc  -Iinclude/ -Wall -pthread $^ -o $@ -L./include

obj/logdb.o:	src/logdb.c
		gcc -Iinclude/ -Wall $< -c -o $@




bin/prueba:	obj/prueba.o  lib/liblogdb.so lib/libcsapp.so
		gcc  -Iinclude/ -Wall -pthread $^ -o $@ -L./include


obj/prueba.o:	src/prueba.c
		gcc -Iinclude/ -Wall $< -c -o $@




bin/csapp: obj/prueba.o  obj/libcsapp.so lib/libcsapp.so
		gcc  -Iinclude/ -Wall $^ -o $@ -L./include





obj/f1Crear.o: src/f1Crear.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f2Elementos.o: src/f2Elementos.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f3Insertar.o: src/f3Insertar.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f4Obtener.o: src/f4Obtener.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f5Remover.o: src/f5Remover.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f6Borrar.o: src/f6Borrar.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f7Claves.o: src/f7Claves.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f8Contiene.o: src/f8Contiene.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f9Valores.o: src/f9Valores.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f10Hash.o: src/f10Hash.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@


obj/conexion.o: src/conexion.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/crear.o: src/crear.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/abrir.o: src/abrir.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/put.o: src/put.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/get.o: src/get.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/eliminar.o: src/eliminar.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/cerrar.o: src/cerrar.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/compactar.o: src/compactar.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@

obj/csapp.o: src/csapp.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@



lib/libhashtab.so:	obj/f1Crear.o obj/f2Elementos.o obj/f3Insertar.o obj/f4Obtener.o obj/f5Remover.o  obj/f6Borrar.o obj/f7Claves.o obj/f8Contiene.o obj/f9Valores.o obj/f10Hash.o
		gcc -shared -o $@ $^
lib/liblogdb.so:	obj/conexion.o obj/crear.o obj/abrir.o obj/put.o obj/get.o  obj/eliminar.o obj/cerrar.o obj/compactar.o
		gcc -shared -o $@ $^

lib/csapp.so: obj/csapp.o
		gcc -shared -o $@ $^

clean:                  
	rm obj/*.o bin/* lib/*
